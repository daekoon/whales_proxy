Rails.application.routes.draw do

  # Routing for unsynced
  get 'unsynced/new'
  get 'unsynced/index'
  get 'unsynced/new', to: 'unsynced#new'
  post 'unsynced/new', to: 'unsynced#create'
  get 'unsynced/commit', to: 'unsynced#commit'
  
  # Routing for whales (which is also the primary page)
  get 'whales/delete', to: 'whales#delete'
  get '/main', to: 'whales#main'
  get '/sync', to: 'whales#sync'
  get '/purgecache', to: 'whales#purge_cache'
  get '/cache_ratio', to: 'whales#cache_ratio'

  
  # Root route redirect
  # Once login is implemented route to login page
  root 'whales#main'
  
  resources :whales
  resources :unsynced
end
