# WHALES PROXY APP

Web application that acts as a proxy to the remote Whales server.
Focuses on minimizing wrong request by utilizing a commit-push cycle
similar to git.

## Getting Started

### Prerequisites

This application is built on Ruby on Rails, with PosgreSQL. This readme file will
guide in running this service on Ubuntu 18.04.2 LTS (Bionic Beaver)

* Ruby version: 2.6.1

* Rails version 5.2.2.1

* PostgreSQL version 11.2s


## Installation Guide

This guide will go through the installation of all required dependencies to running the server on a fresh Ubuntu installation (Ubuntu 18.04.2 LTS (Bionic Beaver))

### Installing Ruby

Open terminal, and type in the following to update the packages index
```
sudo apt update
```

Next, install Ruby by typing
```
sudo apt install ruby-full
```

To verify that the installation was successful, run the following command which will print the ruby version. 
```
ruby -- version
```


### Installing Rails

Open terminal, and type in the following to update the packages index.
```
sudo apt update
```

Install rails by typing
```
gem install rails
```

To verify that the installation was successful, run the following command which will print the rails version. 
```
rails -- version
```
### Installing PostgreSQL

Open terminal, and type in the following to update the packages index.
```
sudo apt update
```
Next, install PostgreSQL by typing the following
```
sudo apt install postgresql postgresql-contrib
```

### Configuring PostgreSQL

Switch over to the postgres account by typing the following.
All subsequent steps in this part (configuration of PostgreSQL) must be done as this user.
```
sudo -i -u postgres
```

Create a new role called whales_proxy using the following command
```
create role whales_proxy with createdb login password 'YOUR_PASSWORD_HERE';
```
Ensure that this role is not a superuser for security purposes. Ensure that this role can create/edit databases.

Create a new db for the whales_proxy role
```
createdb whales_proxy
```

Locate the pg_hba.conf to check the configuration file location.
```
locate pg_hba.conf
```

The output should look as such:
```
/etc/postgresql/11/main/pg_hba.conf
```

Move to the directory mentioned above

```
cd /etc/postgresql/11/main
```

Open the configuration file. 
```
vim pg_hba.conf
```

Update these lines as shown
```
# Database administrative login by Unix domain socket
local   all             postgres                                peer

# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     md5
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
host    all             all             127.0.0.1/32            md5
# IPv6 local connections:
host    all             all             ::1/128                 md5
host    all             all             ::1/128                 md5
# Allow replication connections from localhost, by a user with the
# replication privilege.
local   replication     all                                     peer
host    replication     all             127.0.0.1/32            md5
host    replication     all             ::1/128                 md5
:set mouse=                                                          

```


### Setting up Environment Variables and Configuration

Create local_env.yml in directory config/
Fill in the file as follows:

```
AUTH_TOKEN: "INSERT_YOUR_AUTH_TOKEN_HERE"
WHALES_PROXY_DATABASE_PASSWORD: "INSERT_DB_PASSWORD_HERE"
```


### Finalizing and launhing the service

Install all required gems by typing in the following
```
bundle install
```
Set up production database
```
rake db:create db:migrate RAILS_ENV=production
```

Compile all assets
```
RAILS_ENV=production rails assets:precompile
```

### Launching the service

Now you are ready to launch the service. Launch it with the following command:
```
rails s -e production
```


