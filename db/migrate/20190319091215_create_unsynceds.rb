class CreateUnsynceds < ActiveRecord::Migration[5.2]
  def change
    create_table :unsynceds do |t|
      t.string :name
      t.string :country
      t.string :status

      t.timestamps
    end
  end
end
