require 'test_helper'

class UnsyncedControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get unsynced_new_url
    assert_response :success
  end

end
