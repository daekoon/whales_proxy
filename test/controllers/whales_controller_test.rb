require 'test_helper'

class WhalesControllerTest < ActionDispatch::IntegrationTest

  #Test whether the main page loads
  test "should get main page" do
    get main_url
    assert_response :success
  end

  #Test whether root directory loads
  test "should get root" do
    get '/'
    assert_response :success
  end




end
