class Unsynced < ApplicationRecord

   validates :name,  presence: true, length: { maximum: 50 }
   validates :country, presence: true, length: { maximum: 50 }
   validates :status, presence: true
   validates_uniqueness_of :name

   validate :no_duplicate_name_in_whales
  

  def no_duplicate_name_in_whales
    dupe = Whale.find_by_name(name)
    if dupe != nil
      Rails.logger.debug(name)
      error_msg = ": There exists a whale with the same name\n
                     ID: #{dupe.id}\n
                     Name: #{dupe.name}\n
                     Country: #{dupe.country}"
      errors.add(:name, error_msg.html_safe)
    end
  end
end
