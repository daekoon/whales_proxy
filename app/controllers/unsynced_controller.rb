#Cache used
#1. ("unsynced_all")
#2. ("unsynced_all_page_#{page_number}")
#3. ("unsynced_#{id}")


# Class unsynced is used to handle all changes
# that are going to be commited onto the remote server.

class UnsyncedController < ApplicationController
  




  def new
    @unsynced = Unsynced.new  
  end




  def edit
    cache_request_count_update
    @unsynced = Rails.cache.fetch("unsynced_#{ params[:id] }") { 
                    puts 'No cache found. Generating...';
                    cache_miss_update
                    Unsynced.find(params[:id] ) }
  end




  # Method used to update existing unsynced object with new value
  def update
    @unsynced = Unsynced.find(params[:id])
    
    if @unsynced.update_attributes(unsynced_params)
      # If successful update, delete affected cache and redirect
      Rails.cache.delete "unsynced_all"
      Rails.cache.delete "unsynced_#{params[:id]}"
      flush_page_cache
      redirect_to unsynced_index_url
    
    else #Unsuccessful Update, render page with error message
      render 'edit'
    end
  end
  
  # Unused method because delete endpoint does not exist in remote server
  def destroy

    # Flush page cache before element gets deleted
    flush_page_cache
    if Unsynced.find(params[:id]).destroy
      flash[:success] = "Commit deleted"
      Rails.cache.delete "unsynced_all"
      Rails.cache.delete "unsynced_#{params[:id]}"
    else

    end

    redirect_to unsynced_index_url
  end






  # Method used to push all existing commits to remote server
  # SyncServer class handles HTTP calls, this method handles
  # pre/post processing
  def commit
    syncServer = SyncServer.new

    # Handle commits differently based on type.
    # Currently only type 'Create' exists

    createNew = Unsynced.all.where(["status = 'Create'"])
    Rails.logger.debug("createNew size: #{ createNew.size }")

    if createNew.size == 0
      # No commits to be made, just return to main instead of 
      # wasting calls on sync
      redirect_to main_path
      return
    end

    flush_page_cache
    
    # Create hash object to pass to SyncServer for json call
    createList = {}
    createNew.each do |create|
      createList = {
          :name => create.name,
          :country => create.country
      }
      if syncServer.create_whales(createList)
        Rails.cache.delete "unsynced_#{create.id}"
        create.destroy
      else
        # If fail? Shouldn't fail considering input is checked before
        # Processing, unless connection/server error
      end
    end
    
    # No Delete endpoint exists
    # Unused code for now

    #delete = Unsynced.all.where(["status = 'Delete'"])
    #deleteList = {}
    #delete.each do |temp|
    #deleteList = {:id => temp.id,
    #                :name => temp.name,
    #                :country => temp.country

    #               }
      #implmenet this later
    #  if syncServer.delete_whales(deleteList)
    #
    #  else
    #
    #  end
    #end
    
    #syncServer.create_whales(createList)
    #Rails.logger.debug("List: #{ deleteList } ")
    

    # Since most/all unsynceds are deleted, delete cache
    Rails.cache.delete "unsynced_all"
    flush_page_cache


    redirect_to sync_path


  end








  def index
    
    # Cache hit tracking
    cache_request_count_update

    @unsynced = Rails.cache.fetch("unsynced_all_page_#{params[:page] }") { 
        puts 'No cache found. Generating...'; 
        cache_miss_update;
        Unsynced.paginate(page: params[:page], per_page: 10) }
  end






  def create
    @unsynced = Unsynced.new(unsynced_params)
    # Manual ID Assignment
    # ID of unsynced to either match or be larger than
    # highest ID of whales

    # Using array to get the largest value; 1 if both Whale and Unsynced are
    # empty, or it will get whichever id that is higher
    arr = [1];
    temp = Whale.maximum(:id)
    if temp != nil
      arr << temp.next
    end
    
    temp = Unsynced.maximum(:id)
    if temp != nil
      arr << temp.next
    end

    Rails.logger.debug("New id: #{ arr.max }")

    @unsynced.id = arr.max
    if @unsynced.save
      # Successful save, delete unsynced_all cache since data changed
      Rails.cache.delete "unsynced_all"
      flush_page_cache
      redirect_to unsynced_index_url
    else
      # Unsuccessful save, rerender page with errors
      render 'new'
    end
  end

  private
    # Method used to sanitize parameters hash
    def unsynced_params
      params.require(:unsynced).permit(:name, :country).merge(:status => "Create")
    end
    
    def flush_page_cache
      count = (Unsynced.count/10.0).ceil
      i = 1
      while i <= count
        Rails.cache.delete "unsynced_all_page_#{ i }"
        i += 1
      end
    end
    
    def cache_request_count_update
      cache_request = Metadatum.find_by_name("Cache_Request")
      count = cache_request.value.to_i
      count += 1
      cache_request.update_column(:value, count.to_s)
      cache_request.save

      cache_hit_update(1)

    end

    def cache_hit_update num
      cache_hit = Metadatum.find_by_name("Cache_Hit")
      count = cache_hit.value.to_i
      count += num
      cache_hit.update_column(:value, count.to_s)
      cache_hit.save
    end

    def cache_miss_update
      cache_miss = Metadatum.find_by_name("Cache_Miss")
      count = cache_miss.value.to_i
      count += 1
      cache_miss.update_column(:value, count.to_s)
      cache_miss.save

      cache_hit_update(-1)
    end
  
end
