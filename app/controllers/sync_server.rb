require 'httparty'
require 'json'

# Class used to handle HTTP calls with  remote server

class SyncServer
  
  include HTTParty
  base_uri "https://whalemarket.saleswhale.io"
  format :json
  
  # Store api_key in config/local_env.yml
  def api_key
    ENV['AUTH_TOKEN']
  end

  def base_path
    "/whales"
  end

  def get_all_whales
    url = "#{ base_path }"
    self.class.get(url, { headers: { "Authorization"  => "Bearer #{ api_key }" } } )
  end

  def get_whale_by_id(id)
    url = "#{ base_path }/#{ id }"
    self.class.get(url, { headers: { "Authorization" => "Bearer #{ api_key } " } } )
  end
  

  # Method used to push "Create" type commits
  # Returns true/false depending on success/failure of API
  def create_whales(createList)
    url = "#{ base_path }"
    json = createList.to_json
    Rails.logger.debug("CREATE Payload : #{ json } ")
    response = self.class.post(url, { headers: {"Authorization" => "Bearer #{ api_key } ", 
                                     "Content-Type" => "application/json" },
                               body: json })
    if response.code == 201
      Rails.logger.debug("Successful POST")
      true
    else
      Rails.logger.debug("Unsuccessful POST")
      Rails.logger.debug(response)
      false
    end
  end
  

  # No DELETE endpoint 
  # Unused method
  def delete_whales(deleteList)
    url = "#{ base_path }"
    json = deleteList.to_json
    Rails.logger.debug("DELETE Payload : #{ json } ")
    response = self.class.delete(url, { headers: {"Authorization" => "Bearer #{ api_key }",
                                        "Content-Type" => "application/json" },
                                 boxy: json })
    Rails.logger.debug("DELETE Response: #{ response }")

    if response.code == 201
      Rails.logger.debug("Successful DELETE")
      true
    else
      Rails.logger.debug("Unsuccessful DELETE")
      false
    end
  end


end
