#Cache used:
#1. ("all_whales")
#2. ("all_whales_#{page_number}")
#3. ("whale_#{id}")
#4. ("Last_Sync")

require 'date'


class WhalesController < ApplicationController
  
  # Method used to sync local database and remote database
  # Makes use of the class SyncServer to handle HTTP requests
  # This method mostly handles pre/postprocessing
  def sync
    
    # Keep track of existing whales before syncing
    whales = Whale.all


    syncServer = SyncServer.new
    
    # Obtain response from get/whale endpoint of remote server
    response = syncServer.get_all_whales

    Rails.logger.debug("Response: #{ response }")
    
    # Store the IDs of whales in remote server
    # Used later to remove whales that no longer exist
    remoteID = []

    response["whales"].each do |whale|
      # If whale with id exists, update that whale
      if Whale.exists?(whale["id"])
        temp = Whale.find(whale["id"])
        temp.update_attributes(name: whale["name"], country: whale["country"])

        # Attributes of current whale changed, delete whale specific cache
        Rails.cache.delete "whale_#{ whale["id"] }"

      # If whale with id doesn't exist, create new one
      else
        temp = Whale.new(:id => whale["id"], name: whale["name"], country: whale["country"])
        temp.save
      end
      
      # Add the index of whale into remoteID array
      remoteID << whale["id"]

    end

    # Delete all_whales cache, outdated
    Rails.cache.delete 'all_whales'
    flush_page_cache
        
    # Delete all whales that no longer exist on remote server
    whales.each do |whale|
      if !remoteID.include? whale.id
        whale.destroy
      end
    end

    # Update the last sync time
    temp = Metadatum.find_by name: "Last_Sync"
    curTime = DateTime.parse(Time.now.to_s).strftime("%d/%m/%Y %H:%M")
    temp.update_attribute(:value, curTime)
    Rails.cache.delete "Last_Sync"
    
    # Sync done, redirect to main page
    redirect_to main_url
  end

  #Method used to clear cache
  def purge_cache
    
    #Keep track of reset count
    reset_count = Metadatum.find_by_name("Reset_Count")
    count = reset_count.value.to_i
    count += 1;
    reset_count.update_column(:value, count.to_s)
    reset_count.save

    Rails.cache.clear
    redirect_to main_url
  end
    
  def main
    
    check_first_time

    # Cache hit ratio tracking
    cache_request_count_update 

    @whales = Rails.cache.fetch("all_whales_page_#{ params[:page]}") { 
       puts 'No cache found. Generating...'; 
       cache_miss_update;
       Whale.paginate(page: params[:page], per_page: 10) }
  end

  def show
    
    # Cache hit ratio tracking
    cache_request_count_update

    @whale = Rails.cache.fetch("whale_#{ params[:id] }") { 
        puts 'No cache found. Generating...'; 
        cache_miss_update;
        Whale.find(params[:id]) }
  end

  #Endpoint to show cache ratio
  def cache_ratio
    @cache_hit = Metadatum.find_by_name("Cache_Hit").value
    @cache_miss = Metadatum.find_by_name("Cache_Miss").value
    @cache_request = Metadatum.find_by_name("Cache_Request").value
    @reset_count = Metadatum.find_by_name("Reset_Count").value
  end


  # No delete endpoint available on remote server, ignore this method
  def destroy
    whale = Whale.find(params[:id])
    unSync = Unsynced.new
    unSync.id = whale.id
    unSync.name = whale.name
    unSync.country = whale.country
    unSync.status = "Delete"
    unSync.save
    redirect_to unsynced_index_url
  end
  
  private
    def flush_page_cache
      # Delete all_whales pagination cache
      count = Whale.count
      count = (count/10.0).ceil
      Rails.logger.debug("Current count: ")
      Rails.logger.debug(count)

      i = 1
      while i <= count
        Rails.cache.delete "all_whales_page_#{ i }"
        i += 1
      end
    end

    def check_first_time
      setup = First_Time_Setup.new
      if !setup.check
        Rails.logger.debug("Begin first time setup")
        setup.begin_first_time
      end
    end

    def cache_request_count_update
      cache_request = Metadatum.find_by_name("Cache_Request")
      count = cache_request.value.to_i
      count += 1
      cache_request.update_column(:value, count.to_s)
      cache_request.save
        
      cache_hit_update(1)
      
    end

    def cache_hit_update num
      cache_hit = Metadatum.find_by_name("Cache_Hit")
      count = cache_hit.value.to_i
      count += num
      cache_hit.update_column(:value, count.to_s)
      cache_hit.save
    end

    def cache_miss_update
      cache_miss = Metadatum.find_by_name("Cache_Miss")
      count = cache_miss.value.to_i
      count += 1
      cache_miss.update_column(:value, count.to_s)
      cache_miss.save

      cache_hit_update -1
    end

      
end
