
class First_Time_Setup
  
  # Method that checks whether this is the first time a server is running
  def check

    if Metadatum.find_by_name("First_Time_Setup") == nil
      return false
    else
      return true
    end
  end
  

  # Method to start first time setup
  def begin_first_time
    
    # Create last_sync metadata
    last_sync = Metadatum.new(name: "Last_Sync", value: "Never").save

    # Create first_time_setup metadata
    Metadatum.new(name: "First_Time_Setup", value: "Done").save

    # Create Cache_Hit metadata
    Metadatum.new(name: "Cache_Hit", value:"0").save

    # Create Cache_Miss metadata
    Metadatum.new(name: "Cache_Miss", value:"0").save

    # Create Cache_Request metadata
    Metadatum.new(name: "Cache_Request", value:"0").save

    # Create Reset_Count metadata
    Metadatum.new(name: "Reset_Count", value:"0").save
    


  end


end
