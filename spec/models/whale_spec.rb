require 'rails_helper'

describe Whale do
  
  it "can save valid whales" do
    temp = Whale.new(name:"rspectest", country:"rspectest")
    expect(temp.save).to eq(true)
  end


  it "Name must exist" do
    temp = Whale.new(country: "test")
    expect(temp.valid?).to eq(false)
  end

  it "Country must exist" do
    temp = Whale.new(name: "test")
    expect(temp.valid?).to eq(false)
  end
    
end
