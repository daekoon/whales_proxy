require 'rails_helper'

describe Unsynced do
  
  it "can save valid unsynced" do
    temp = Unsynced.new(name: "rspectest", country: "rspectest", status: "Create")
    expect(temp.save).to eq(true)
  end


  it "Name must exist" do
    temp = Unsynced.new(country: "test", status: "Create")
    expect(temp.valid?).to eq(false)
  end

  it "Country must exist" do
    temp = Unsynced.new(name: "test", status: "Create")
    expect(temp.valid?).to eq(false)
  end

  it "Status must exist" do
    temp = Unsynced.new(name: "test", country: "rspectest")
    expect(temp.valid?).to eq(false)
  end
  
  it "Name must not exceed 50 charcters" do
    string = "";
    51.times do
      string = string + "a"
    end
    temp = Unsynced.new(name: "#{string}", country: "rspectest", status: "Create")
    expect(temp.valid?).to eq(false)
  end

  it "Country must not exceed 50 charcters" do
    string = "";
    51.times do
      string = string + "a"
    end
    temp = Unsynced.new(name: "rspectest", country: "#{string}", status: "Create")
    expect(temp.valid?).to eq(false)
  end

  it "No duplicate name within unsynced" do
    Unsynced.new(name: "test", country: "rspectest", status: "Create").save
    temp = Unsynced.new(name: "test", country: "rspectest", status: "Create")
    expect(temp.valid?).to eq(false)
  end
  
  it "No duplicate name with whales" do
    Whale.new(name: "test", country: "rspectest").save
    temp = Unsynced.new(name: "test", country: "rspectest", status: "Create")
    expect(temp.valid?).to eq(false)
  end


end
